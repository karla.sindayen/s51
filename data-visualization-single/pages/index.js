import { useState, useEffect } from 'react'
import { Form, Button } from 'react-bootstrap'
import Head from 'next/head'
import styles from '../styles/Home.module.css'
import PieChart from '../components/PieChart'

export default function Home() {

    //creates a reference to the file input form element
    const fileRef = React.createRef()

    //state for csv data
    const [csvData, setCsvData] = useState([]) // storing info

    const [brands, setBrands] = useState([])
    const [salesByBrand, setSalesByBrand] = useState([])

    //function to convert csv file to base64
    const toBase64 = (file) => new Promise((resolve, reject) => { 
        //reading info as url
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => resolve(reader.result)
        reader.onerror = error => reject(error)
    })

    function uploadCSV(e){
      e.preventDefault()

      //0 - first file to upload
      toBase64(fileRef.current.files[0])
      .then(encodedFile => {

          fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}/api/cars`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({csvData: encodedFile})
          })
          .then(res => res.json())
          .then(data => {
            console.log(data)

              if(data.jsonArray.length > 0){
                  setCsvData(data.jsonArray)
              }
          })
      })
    }

    useEffect(() => {

        let makes = []

        csvData.forEach(element => {
            //current obj doesnt exist,  save 
            if(!makes.find(make => make === element.make)){
              makes.push(element.make)
            }
        }, [csvData])

        console.log(makes)

        setBrands(makes)

    }, [csvData])

    useEffect(() => {
        setSalesByBrand(brands.map(brand => {

            let sales = 0

            //compare brand
            csvData.forEach(element => {
                if(element.make === brand){
                    sales = sales + parseInt(element.sales)
                }
            })

            return {
                brand: brand,
                sales: sales
            }
        }))

    }, [brands])
    
            console.log(salesByBrand)

    return (
        <React.Fragment>
            <Head>
                <title>CSV Data Visualization</title>
            </Head>
            {csvData.length > 0 ? <PieChart carData={salesByBrand} /> : null}
            <Form onSubmit={(e) => uploadCSV(e)}>
                <Form.Group controlId="csvUploadForm">
                    <Form.Label>Upload CSV</Form.Label>
                    <Form.Control type="file" ref={ fileRef } accept="csv" required />
                </Form.Group>
                    <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
            </Form>
        </React.Fragment>
    )
}
