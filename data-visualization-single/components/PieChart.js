import { useState, useEffect } from 'react'
import { Pie } from 'react-chartjs-2'
import { colorRandomizer } from '../helpers'

export default function PieChart({ carData }){

	// carData = array of obj [
	// 	{
	// 		brand: "Dodge",
	// 		sales: 47
	// 	}
	// ]

	const [brands, setBrands] = useState([])
	const [sales, setSales] = useState([])
	const [bgColors, setBgColors] = useState([])

	useEffect(() => {
		if(carData.length > 0){
			setBrands(carData.map(element => element.brand))
			setSales(carData.map(element => element.sales))
			setBgColors(carData.map(() => `#${colorRandomizer()}`))

			carData.sort((a,b) => b.sales - a.sales)
		}
	}, [carData])

	const data = {
		labels: brands, 
		datasets: [{
			data: sales,
			backgroundColor: bgColors,
			hoverBackgroundColor: bgColors
		}]
	}

	return (
		<Pie data={data} />
	)
}